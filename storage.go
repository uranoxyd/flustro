// Copyright (c) 2021 David Ewelt <uranoxyd@gmail.com>, released under MIT License. See LICENSE file.

package main

import (
	"errors"
	"fmt"
	"os"
	"sync"

	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
)

var ErrDocumentNotFound error = errors.New("document not found")
var ErrUnmarshalDocument error = errors.New("error unmarshalling document")
var ErrMarshalDocument error = errors.New("error marshalling document")

var storage *Storage

func init() {
	DefaultConfig.Storage = &StorageConfig{
		MaxTargets:     25,
		MaxTitleLength: 100,
		MaxBodyLength:  2000,
		Adapter:        "sqlite",
		Config: &SQLiteStorageAdapterConfig{
			DatabasePath: "flustro.db",
		},
	}
}

type StorageAdapterError struct {
	Err error
}

func (e *StorageAdapterError) Error() string {
	return fmt.Sprintf("error in storage adapter: %v", e.Err)
}
func (e *StorageAdapterError) Unwrap() error {
	return e.Err
}

type Storage struct {
	Adapter StorageAdapter `json:"-"`
	mutex   sync.Mutex     `json:"-"`
}

func (storage *Storage) Startup() error {
	if err := storage.Adapter.Startup(); err != nil {
		log.Errorf("storage adaper error in Startup(): %v", err)
		return &StorageAdapterError{Err: err}
	}
	return nil
}
func (storage *Storage) Shutdown() error {
	if err := storage.Adapter.Shutdown(); err != nil {
		log.Errorf("storage adaper error in Shutdown(): %v", err)
		return &StorageAdapterError{Err: err}
	}
	return nil
}
func (storage *Storage) GetHashType(hash string) (string, error) {
	storage.mutex.Lock()
	defer storage.mutex.Unlock()

	if hashType, err := storage.Adapter.GetHashType(hash); err != nil {
		log.Errorf("storage adaper error in GetHashType(): %v", err)
		return "", &StorageAdapterError{Err: err}
	} else {
		return hashType, nil
	}
}
func (storage *Storage) GetTargetName(hash string) (string, error) {
	storage.mutex.Lock()
	defer storage.mutex.Unlock()

	if targetName, err := storage.Adapter.GetTargetName(hash); err != nil {
		log.Errorf("storage adaper error in GetTargetName(): %v", err)
		return "", &StorageAdapterError{Err: err}
	} else {
		return targetName, nil
	}
}
func (storage *Storage) GetAllTargetNames() (TargetList, error) {
	storage.mutex.Lock()
	defer storage.mutex.Unlock()

	if targetNames, err := storage.Adapter.GetAllTargetNames(); err != nil {
		log.Errorf("storage adaper error in GetAllTargetNames(): %v", err)
		return nil, &StorageAdapterError{Err: err}
	} else {
		targetNames.Sort()
		return targetNames, nil
	}
}
func (storage *Storage) UpdateTargetHash(oldHash string, newHash string) error {
	storage.mutex.Lock()
	defer storage.mutex.Unlock()

	if err := storage.Adapter.UpdateTargetHash(oldHash, newHash); err != nil {
		log.Errorf("storage adaper error in UpdateTargetHash(): %v", err)
		return &StorageAdapterError{Err: err}
	}
	return nil
}
func (storage *Storage) WriteDocument(document *Document) error {
	storage.mutex.Lock()
	defer storage.mutex.Unlock()

	hash := document.GetHash()
	if err := storage.Adapter.WriteDocument(hash, document); err != nil {
		log.Errorf("storage adaper error in WriteDocument(): %v", err)
		return &StorageAdapterError{Err: err}
	}
	return nil
}
func (storage *Storage) GetAllDocuments(startTime int64, endTime int64, limit int) ([]*Document, error) {
	storage.mutex.Lock()
	defer storage.mutex.Unlock()

	var documents []*Document = make([]*Document, 0)
	documentHashes, err := storage.Adapter.GetAllDocuments(startTime, endTime, limit)
	if err != nil {
		log.Errorf("storage adaper error in GetAllDocuments(): %v", err)
		return nil, &StorageAdapterError{Err: err}
	}
	for _, documentHash := range documentHashes {
		document, err := storage.Adapter.ReadDocument(documentHash)
		if err != nil {
			log.Errorf("storage adaper error in ReadDocument(): %v", err)
			return nil, &StorageAdapterError{Err: err}
		}
		document.Targets.Sort()
		documents = append(documents, document)
	}
	return documents, nil
}
func (storage *Storage) GetDocumentsForTarget(targetHash string, startTime int64, endTime int64, limit int) ([]*Document, error) {
	storage.mutex.Lock()
	defer storage.mutex.Unlock()

	var documents []*Document = make([]*Document, 0)
	documentHashes, err := storage.Adapter.GetDocumentsForTarget(targetHash, startTime, endTime, limit)
	if err != nil {
		log.Errorf("storage adaper error in GetDocumentsForTarget(): %v", err)
		return nil, &StorageAdapterError{Err: err}
	}
	for _, documentHash := range documentHashes {
		document, err := storage.Adapter.ReadDocument(documentHash)
		if err != nil {
			log.Errorf("storage adaper error in ReadDocument(): %v", err)
			return nil, &StorageAdapterError{Err: err}
		}
		document.Targets.Sort()
		documents = append(documents, document)
	}
	return documents, nil
}
func (storage *Storage) GetDocument(hash string) (*Document, error) {
	storage.mutex.Lock()
	defer storage.mutex.Unlock()

	document, err := storage.Adapter.ReadDocument(hash)
	if err != nil {
		log.Errorf("storage adaper error in ReadDocument(): %v", err)
		return nil, &StorageAdapterError{Err: err}
	}
	document.Targets.Sort()
	return document, nil
}
func (storage *Storage) SaveDocument(document *Document) error {
	storage.mutex.Lock()
	defer storage.mutex.Unlock()

	err := storage.Adapter.WriteDocument(document.GetHash(), document)
	if err != nil {
		log.Errorf("storage adaper error in WriteDocument(): %v", err)
		return &StorageAdapterError{Err: err}
	}
	return nil
}
func (storage *Storage) DeleteDocument(document *Document) error {
	storage.mutex.Lock()
	defer storage.mutex.Unlock()

	err := storage.Adapter.DeleteDocument(document.GetHash())
	if err != nil {
		log.Errorf("storage adaper error in DeleteDocument(): %v", err)
		return &StorageAdapterError{Err: err}
	}
	return nil
}

func NewStorage(adapter StorageAdapter) *Storage {
	return &Storage{
		Adapter: adapter,
	}
}

func initializeStorage() error {
	storageAdapter, storageAdapterConfig, err := storageAdapterFactory(config.Storage.Adapter)
	if err != nil {
		return err
	}

	if storageAdapterConfig != nil {
		decoderConfig := &mapstructure.DecoderConfig{Result: &storageAdapterConfig, DecodeHook: mapstructure.StringToTimeDurationHookFunc()}

		decoder, err := mapstructure.NewDecoder(decoderConfig)
		if err != nil {
			return err
		}
		err = decoder.Decode(config.Storage.Config)
		if err != nil {
			return err
		}

		config.Storage.Config = storageAdapterConfig
	}

	storage = NewStorage(storageAdapter)

	return nil
}

func storageMain() {
	if err := initializeStorage(); err != nil {
		log.Errorf("error initializing storage: %v", err)
		os.Exit(1)
	}
}
