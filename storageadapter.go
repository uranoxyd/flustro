// Copyright (c) 2021 David Ewelt <uranoxyd@gmail.com>, released under MIT License. See LICENSE file.

package main

import (
	"fmt"
)

type StorageAdapter interface {
	Startup() error
	Shutdown() error
	GetHashType(hash string) (string, error)
	GetAllTargetNames() (TargetList, error)
	GetAllDocuments(startTime int64, endTime int64, limit int) ([]string, error)
	GetDocumentsForTarget(targetHash string, startTime int64, endTime int64, limit int) ([]string, error)
	UpdateTargetHash(oldHash string, newHash string) error
	ReadDocument(hash string) (*Document, error)
	WriteDocument(hash string, document *Document) error
	DeleteDocument(hash string) error
	GetTargetName(hash string) (string, error)
}

type StorageAdapterRegistration struct {
	Name    string
	Adapter StorageAdapter
	Config  interface{}
}

var storageAdapterRegistry map[string]*StorageAdapterRegistration = make(map[string]*StorageAdapterRegistration)

func storageAdapterFactory(name string) (StorageAdapter, interface{}, error) {
	registration := storageAdapterRegistry[name]

	if registration == nil {
		return nil, nil, fmt.Errorf("'%s' is not a registered storage adapter", name)
	}

	return registration.Adapter, registration.Config, nil
}

func RegisterStorageAdapter(name string, adapter StorageAdapter, config interface{}) {
	storageAdapterRegistry[name] = &StorageAdapterRegistration{
		Name:    name,
		Adapter: adapter,
		Config:  config,
	}
}
