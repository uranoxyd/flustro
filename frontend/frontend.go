// Copyright (c) 2021 David Ewelt <uranoxyd@gmail.com>, released under MIT License. See LICENSE file.

package frontend

import (
	"bufio"
	"bytes"
	"io"
	"io/fs"
	"net/http"
	"strings"
	"text/template"

	log "github.com/sirupsen/logrus"
	"gitlab.com/uranoxyd/mergingfs"
)

type TemplateMeta struct {
	Content  []byte
	Path     string
	Requires []*TemplateMeta
}

type FrontendServer struct {
	address    string
	templateFS fs.FS

	cacheTemplates    bool
	templateFunctions template.FuncMap
	templateMeta      map[string]*TemplateMeta
	templates         map[string]*template.Template
	handler           http.Handler
}

func (server *FrontendServer) loadTemplate(path string) (*TemplateMeta, error) {
	content, err := fs.ReadFile(server.templateFS, path)
	if err != nil {
		return nil, err
	}

	scanner := bufio.NewScanner(bytes.NewReader(content))
	trimContent := 0
	requires := make([]*TemplateMeta, 0)
	for scanner.Scan() {
		line := strings.Trim(scanner.Text(), " ")
		if strings.ToLower(line[:8]) != "@require" {
			break
		}
		trimContent += len(line) + 1

		requiredMeta, err := server.loadTemplate(line[10:])
		if err != nil {
			return nil, err
		}
		requires = append(requires, requiredMeta)
	}

	meta := &TemplateMeta{
		Path:     path,
		Content:  bytes.TrimLeft(content[trimContent:], "\r\n"),
		Requires: requires,
	}

	server.templateMeta[path] = meta

	return meta, nil
}

func (server *FrontendServer) loadTemplates() {
	mergingfs.WalkFS(server.templateFS, ".", func(fsys fs.FS, path string) {
		_, err := server.loadTemplate(path)
		if err != nil {
			log.Errorf("error reading template '%s': %v", path, err)
			return
		}
	})
}

func (server *FrontendServer) getOrParseTemplate(path string) *template.Template {
	tmpl, found := server.templates[path]
	if !found || !server.cacheTemplates {
		var meta *TemplateMeta
		if !server.cacheTemplates {
			var err error
			meta, err = server.loadTemplate(path)
			if err != nil {
				log.Errorf("error reading template '%s': %v", path, err)
				return nil
			}
		} else {
			meta = server.templateMeta[path]
		}

		tmpl = template.New("")

		for _, requires := range meta.Requires {
			t := tmpl.New(requires.Path).Funcs(server.templateFunctions)
			if _, err := t.Parse(string(requires.Content)); err != nil {
				log.Errorf("error parsing template %s: %v", requires, err)
			}
		}

		t := tmpl.New(path).Funcs(server.templateFunctions)
		if _, err := t.Parse(string(meta.Content)); err != nil {
			log.Errorf("error parsing template %s: %v", path, err)
		}

		server.templates[path] = tmpl
	}

	return tmpl
}

func (server *FrontendServer) ExecuteTemplate(w io.Writer, data interface{}, name string) error {
	tmpl := server.getOrParseTemplate(name)
	err := tmpl.ExecuteTemplate(w, name, data)
	if err != nil {
		log.Errorf("error executing template '%s': %v", name, err)
		return err
	}
	return nil
}

func (server *FrontendServer) AddTemplateFunction(name string, function interface{}) {
	server.templateFunctions[name] = function
}

func (server *FrontendServer) Start() error {
	server.loadTemplates()

	httpServer := &http.Server{Addr: server.address, Handler: server.handler}
	return httpServer.ListenAndServe()
}

func NewFrontendServer(address string, templateFS fs.FS, handler http.Handler) *FrontendServer {
	return &FrontendServer{
		address:           address,
		templateFS:        templateFS,
		templateFunctions: make(template.FuncMap),
		templateMeta:      make(map[string]*TemplateMeta),
		templates:         make(map[string]*template.Template),
		handler:           handler,
	}
}
