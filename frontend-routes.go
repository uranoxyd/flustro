// Copyright (c) 2021 David Ewelt <uranoxyd@gmail.com>, released under MIT License. See LICENSE file.

package main

import (
	"errors"
	"fmt"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

func frontendExecuteErrorTemplate(w http.ResponseWriter, r *http.Request, statusCode int, err error) {
	context := new(struct {
		Editor string
		Error  error
	})
	context.Editor = frontendCheckAuthorization(r)
	context.Error = err

	w.WriteHeader(statusCode)
	frontendServer.ExecuteTemplate(w, context, "error.html")
}

//--
//-- Index
//--

func frontendIndex(w http.ResponseWriter, r *http.Request) {
	editor := frontendCheckAuthorization(r)

	targets, err := storage.GetAllTargetNames()
	if err != nil {
		frontendExecuteErrorTemplate(w, r, http.StatusInternalServerError, errors.New("could not get target names"))
		return
	}

	nonTargetDocuments, err := storage.GetDocumentsForTarget("", 0, 0, 0)
	if err != nil {
		frontendExecuteErrorTemplate(w, r, http.StatusInternalServerError, errors.New("could not get non-target documents"))
		return
	}

	context := new(struct {
		Config             *Config
		Editor             string
		Targets            []string
		NonTargetDocuments []*Document
	})
	context.Config = config
	context.Editor = editor
	context.Targets = targets
	context.NonTargetDocuments = nonTargetDocuments

	frontendServer.ExecuteTemplate(w, context, "index.html")
}

//--
//-- Login / Logout
//--

func frontendLogin(w http.ResponseWriter, r *http.Request) {
	frontendRequireAuthorization(w, r, "/")
}

func frontendLogout(w http.ResponseWriter, r *http.Request) {
	cookie := &http.Cookie{Name: "flustro-login", Expires: time.Now().Add(-24 * time.Hour), Secure: true}
	http.SetCookie(w, cookie)

	w.Header().Set("Location", "/")
	w.WriteHeader(http.StatusSeeOther)
}

//--
//-- Query
//--

func frontendQuery(w http.ResponseWriter, r *http.Request) {
	query := mux.Vars(r)["query"]
	dot := strings.Index(query, ".")
	var hash string = query
	var format string = "html"
	if dot > -1 {
		hash = query[:dot]
		format = strings.ToLower(query[dot+1:])
	}

	hashType, err := storage.Adapter.GetHashType(hash)
	if err != nil {
		frontendExecuteErrorTemplate(w, r, http.StatusNotFound, errors.New("unknown target"))
		return
	}

	log.Tracef("request for %s (type=%s) with format %s", hash, hashType, format)

	switch hashType {
	case "document":
		frontendQueryDocument(w, r, hash, format)
	case "target":
		frontendQueryDocumentList(w, r, hash, format)
	default:
		frontendExecuteErrorTemplate(w, r, http.StatusNotFound, errors.New("unknown target"))
	}
}

func frontendQueryDocumentList(w http.ResponseWriter, r *http.Request, targetHash string, format string) {
	switch format {
	case "html":
		frontendQueryDocumentListTemplated(w, r, targetHash, "document-list.html", "text/html")
	case "rss":
		frontendQueryDocumentListTemplated(w, r, targetHash, "rss-feed.xml", "application/rss+xml")
	case "atom":
		frontendQueryDocumentListTemplated(w, r, targetHash, "atom-feed.xml", "application/atom+xml")
	default:
		frontendExecuteErrorTemplate(w, r, http.StatusBadRequest, errors.New("unknown format"))
	}
}

func frontendQueryDocumentListTemplated(w http.ResponseWriter, r *http.Request, targetHash string, template string, contentType string) {
	targetName, err := storage.GetTargetName(targetHash)
	if err != nil {
		frontendExecuteErrorTemplate(w, r, http.StatusBadRequest, err)
		return
	}

	w.Header().Set("Content-Type", contentType)
	editor := frontendCheckAuthorization(r)

	yearStr := mux.Vars(r)["year"]
	monthStr := mux.Vars(r)["month"]
	isOverview := true

	var previousMonth = time.Now().AddDate(0, -1, 0)
	var nextMonth = time.Now().AddDate(0, 1, 0)
	var monthStart time.Time = time.Unix(0, 0)
	var monthEnd time.Time = time.Unix(0, 0)
	limit := config.Frontend.HTML.MaxEntriesOnOverview
	if yearStr != "" && monthStr != "" {
		isOverview = false
		limit = 0
		year, err := strconv.Atoi(yearStr)
		if err != nil {
			frontendExecuteErrorTemplate(w, r, http.StatusBadRequest, errors.New("cannot parse year"))
			return
		}
		month, err := strconv.Atoi(monthStr)
		if err != nil {
			frontendExecuteErrorTemplate(w, r, http.StatusBadRequest, errors.New("cannot parse month"))
			return
		}

		monthStart = time.Date(year, time.Month(month), 1, 0, 0, 0, 0, time.Local)
		monthEnd = monthStart.AddDate(0, 1, 0)
		previousMonth = monthStart.AddDate(0, -1, 0)
		nextMonth = monthStart.AddDate(0, 1, 0)
	}

	documents, err := storage.GetDocumentsForTarget(targetHash, monthStart.Unix(), monthEnd.Unix(), limit)
	if err != nil {
		frontendExecuteErrorTemplate(w, r, http.StatusBadRequest, err)
		return
	}

	var lastUpdate time.Time
	if len(documents) > 0 {
		lastUpdate = documents[0].Created
	}

	if isOverview && len(documents) > config.Frontend.HTML.MaxEntriesOnOverview {
		documents = documents[:config.Frontend.HTML.MaxEntriesOnOverview]
	}

	var documentGroups map[time.Time]*DocumentDateGroup = make(map[time.Time]*DocumentDateGroup)
	var documentGroupsFlat []*DocumentDateGroup
	for _, document := range documents {
		groupTime := document.Created.Truncate(time.Hour * 24)
		group, found := documentGroups[groupTime]
		if !found {
			group = &DocumentDateGroup{
				Date:      groupTime,
				Documents: make([]*Document, 0),
			}
			documentGroups[groupTime] = group
			documentGroupsFlat = append(documentGroupsFlat, group)
		}
		group.Documents = append(group.Documents, document)
	}

	sort.SliceStable(documents, func(i, j int) bool {
		return documents[i].Created.Unix() > documents[j].Created.Unix()
	})

	context := new(struct {
		Config         *Config
		Editor         string
		IsCurrentMonth bool
		PreviousYear   int
		PreviousMonth  int
		NextYear       int
		NextMonth      int
		LastUpdate     time.Time
		TargetHash     string
		TargetName     string
		Documents      []*Document
		DocumentGroups []*DocumentDateGroup
	})
	context.Config = config
	context.Editor = editor
	context.PreviousYear = previousMonth.Year()
	context.PreviousMonth = int(previousMonth.Month())
	context.NextYear = nextMonth.Year()
	context.NextMonth = int(nextMonth.Month())
	context.IsCurrentMonth = monthStart.Unix() == 0 || (monthStart.Year() == time.Now().Year() && monthStart.Month() == time.Now().Month())
	context.LastUpdate = lastUpdate
	context.TargetHash = targetHash
	context.TargetName = targetName
	context.Documents = documents
	context.DocumentGroups = documentGroupsFlat

	frontendServer.ExecuteTemplate(w, context, template)
}

func frontendQueryDocument(w http.ResponseWriter, r *http.Request, documentHash string, format string) {
	switch format {
	case "html":
		frontendQueryDocumentHTML(w, r, documentHash)
	default:
		frontendExecuteErrorTemplate(w, r, http.StatusBadRequest, errors.New("unknown format"))
	}
}

func frontendQueryDocumentHTML(w http.ResponseWriter, r *http.Request, documentHash string) {
	document, err := storage.GetDocument(documentHash)
	if err != nil {
		if err == ErrDocumentNotFound {
			frontendExecuteErrorTemplate(w, r, http.StatusNotFound, errors.New("Document not found"))
			return
		}
		frontendExecuteErrorTemplate(w, r, http.StatusInternalServerError, errors.New("could not load document"))
		return
	}

	editor := frontendCheckAuthorization(r)

	context := new(struct {
		Document *Document
		Editor   string
	})
	context.Document = document
	context.Editor = editor

	frontendServer.ExecuteTemplate(w, context, "document.html")
}

//--
//-- Masterfeed
//--

func frontendMasterFeed(w http.ResponseWriter, r *http.Request) {
	editor := frontendRequireAuthorization(w, r, "")
	if editor == "" {
		frontendExecuteErrorTemplate(w, r, http.StatusUnauthorized, errors.New("uanthorized"))
		return
	}

	allowed := false
	for _, user := range config.AllowMasterFeedFor {
		if strings.EqualFold(user, editor) {
			allowed = true
			break
		}
	}

	if !allowed {
		frontendExecuteErrorTemplate(w, r, http.StatusUnauthorized, errors.New("uanthorized"))
		return
	}

	format := mux.Vars(r)["format"]

	documents, err := storage.GetAllDocuments(0, 0, 0)
	if err != nil {
		frontendExecuteErrorTemplate(w, r, http.StatusInternalServerError, errors.New("could not load documents"))
		return
	}

	var lastUpdate time.Time = time.Now()
	if len(documents) > 0 {
		lastUpdate = documents[0].Created
	}

	context := new(struct {
		Config     *Config
		Editor     string
		TargetHash string
		TargetName string
		LastUpdate time.Time
		Documents  []*Document
	})
	context.Config = config
	context.TargetName = "Master Feed"
	context.TargetHash = "master-feed"
	context.LastUpdate = lastUpdate
	context.Documents = documents

	switch format {
	case "rss":
		w.Header().Set("Content-Type", "application/rss+xml")
		frontendServer.ExecuteTemplate(w, context, "rss-feed.xml")
	case "atom":
		w.Header().Set("Content-Type", "application/atom+xml")
		frontendServer.ExecuteTemplate(w, context, "atom-feed.xml")
	}
}

//--
//-- Edit
//--

func frontendEdit(w http.ResponseWriter, r *http.Request) {
	editor := frontendRequireAuthorization(w, r, "")
	if editor == "" {
		frontendExecuteErrorTemplate(w, r, http.StatusUnauthorized, errors.New("uanthorized"))
		return
	}

	r.ParseForm()

	availableTargets, err := storage.GetAllTargetNames()
	if err != nil {
		frontendExecuteErrorTemplate(w, r, http.StatusInternalServerError, errors.New("could not load targets"))
		return
	}

	hash := mux.Vars(r)["hash"]

	targets := r.URL.Query()["target"]

	context := new(struct {
		Config           *Config
		Editor           string
		Hash             string
		Title            string
		Body             string
		Targets          TargetList
		AvailableTargets TargetList
		Errors           []string
	})
	context.Config = config
	context.Editor = editor
	context.Targets = targets
	context.Hash = hash
	context.AvailableTargets = availableTargets

	var document *Document
	if hash == "" {
		document = &Document{
			Created: time.Now(),
		}
	} else {
		var err error
		document, err = storage.GetDocument(context.Hash)
		if err != nil {
			if err == ErrDocumentNotFound {
				frontendServer.ExecuteTemplate(w, nil, "document-not-found.html")
				return
			}
			frontendExecuteErrorTemplate(w, r, http.StatusInternalServerError, errors.New("could not load document"))
			return
		}
		context.Title = document.Title
		context.Body = document.Body
		context.Targets = document.Targets

		for _, target := range context.Targets {
			context.AvailableTargets.Remove(target)
		}
	}

	if r.Method == "POST" {
		// r.ParseForm()

		context.Hash = r.FormValue("hash")
		context.Title = strings.Trim(r.FormValue("title"), " ")
		context.Body = strings.Trim(r.FormValue("body"), " \n")
		context.Targets = make(TargetList, 0)
		for _, target := range strings.Split(r.FormValue("targets"), ",") {
			target = strings.Trim(target, " ")
			if len(target) == 0 || context.Targets.Contains(target) {
				continue
			}
			context.Targets = append(context.Targets, target)
			context.AvailableTargets.Remove(target)
		}

		if r.FormValue("starget") != "" {
			if !context.Targets.Contains(r.FormValue("starget")) {
				context.Targets = append(context.Targets, r.FormValue("starget"))
				context.AvailableTargets.Remove(r.FormValue("starget"))
			}

			frontendServer.ExecuteTemplate(w, context, "edit.html")
			return
		}

		if context.Title == "" {
			context.Errors = append(context.Errors, "Please enter a title")
		}
		if len(context.Title) > config.Storage.MaxTitleLength {
			context.Errors = append(context.Errors, fmt.Sprintf("Title is to long, maximum is %d chars", config.Storage.MaxTitleLength))
		}
		if context.Body == "" {
			context.Errors = append(context.Errors, "Please enter a body")
		}
		if len(context.Body) > config.Storage.MaxBodyLength {
			context.Errors = append(context.Errors, fmt.Sprintf("Body is to long, maximum is %d chars", config.Storage.MaxBodyLength))
		}
		if len(context.Targets) > config.Storage.MaxTargets {
			context.Errors = append(context.Errors, fmt.Sprintf("Too many targets, maxmimum is %d", config.Storage.MaxTargets))
		}

		if len(context.Errors) == 0 {
			document.Title = context.Title
			document.Body = context.Body
			document.Targets = context.Targets
			document.LastUpdate = time.Now()
			document.Save()

			w.Header().Set("Location", "/")
			w.WriteHeader(http.StatusSeeOther)
			return
		}
	}

	frontendServer.ExecuteTemplate(w, context, "edit.html")
}

//--
//-- Delete
//--

func frontendDelete(w http.ResponseWriter, r *http.Request) {
	editor := frontendRequireAuthorization(w, r, "")
	if editor == "" {
		frontendExecuteErrorTemplate(w, r, http.StatusUnauthorized, errors.New("uanthorized"))
		return
	}

	hash := mux.Vars(r)["hash"]

	document, err := storage.GetDocument(hash)
	if err != nil {
		if err == ErrDocumentNotFound {
			frontendServer.ExecuteTemplate(w, nil, "document-not-found.html")
			return
		}
		frontendExecuteErrorTemplate(w, r, http.StatusInternalServerError, errors.New("could not load document"))
		return
	}

	if r.Method == "POST" {
		r.ParseForm()
		if r.FormValue("confirmed") == "" {
			w.Header().Set("Location", "/"+document.Hash)
			w.WriteHeader(http.StatusSeeOther)
			return
		}

		document.Delete()

		w.Header().Set("Location", "/")
		w.WriteHeader(http.StatusSeeOther)
		return
	}

	context := new(struct {
		Config   *Config
		Editor   string
		Document *Document
	})
	context.Config = config
	context.Editor = editor
	context.Document = document

	frontendServer.ExecuteTemplate(w, context, "delete-document-confirmation.html")
}
