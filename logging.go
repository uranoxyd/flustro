// Copyright (c) 2021 David Ewelt <uranoxyd@gmail.com>, released under MIT License. See LICENSE file.

package main

import (
	"os"
	"time"

	log "github.com/sirupsen/logrus"
)

var logLevelNames map[string]log.Level = map[string]log.Level{
	"panic":   log.PanicLevel,
	"fatal":   log.FatalLevel,
	"error":   log.ErrorLevel,
	"warn":    log.WarnLevel,
	"warning": log.WarnLevel,
	"info":    log.InfoLevel,
	"debug":   log.DebugLevel,
	"trace":   log.TraceLevel,
}

func loggingInit() {
	log.SetOutput(os.Stdout)
	log.SetFormatter(&log.TextFormatter{
		ForceColors:     true,
		FullTimestamp:   true,
		TimestampFormat: time.RFC822,
	})
}

func loggingMain() {
	if level, found := logLevelNames[config.LogLevel]; found {
		log.SetLevel(level)
	} else {
		log.Infof("unknown log level '%s'", config.LogLevel)
	}
}
