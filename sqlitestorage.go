// Copyright (c) 2021 David Ewelt <uranoxyd@gmail.com>, released under MIT License. See LICENSE file.

package main

import (
	"database/sql"
	"encoding/json"
	"math"

	log "github.com/sirupsen/logrus"
	_ "modernc.org/sqlite"
)

func init() {
	RegisterStorageAdapter("sqlite", &SQLiteStorageAdapter{}, &SQLiteStorageAdapterConfig{
		DatabasePath: "flustro.db",
	})
}

type SQLiteStorageAdapterConfig struct {
	DatabasePath string `mapstructure:"db-path"`
}

type SQLiteStorageAdapter struct {
	config  *SQLiteStorageAdapterConfig
	client  *sql.DB
	version int
}

func (adapter *SQLiteStorageAdapter) checkForAndDeleteUnusedTargets() error {
	rows, err := adapter.client.Query("SELECT hash FROM targets t WHERE (SELECT 1 FROM documentTargets dt WHERE dt.targetHash=t.hash) IS NULL")
	if err != nil {
		log.Errorf("error finding unused targets: %v", err)
		return err
	}

	var unusedTargetHashes []string
	for rows.Next() {
		var hash string
		if err := rows.Scan(&hash); err != nil {
			log.Errorf("error finding unused targets: %v", err)
			return err
		}
		log.Infof("unused target: %s", hash)

		unusedTargetHashes = append(unusedTargetHashes, hash)
	}

	for _, hash := range unusedTargetHashes {
		adapter.client.Exec("DELETE FROM targets WHERE hash=?", hash)
		adapter.client.Exec("DELETE FROM hashes WHERE hash=?", hash)
	}

	return nil
}
func (adapter *SQLiteStorageAdapter) Startup() error {
	adapter.config = config.Storage.Config.(*SQLiteStorageAdapterConfig)

	db, err := sql.Open("sqlite", adapter.config.DatabasePath)
	if err != nil {
		return err
	}

	db.Exec("CREATE TABLE IF NOT EXISTS `version` ( `version` INTEGER )")
	db.Exec("CREATE TABLE IF NOT EXISTS `documents` ( `hash` TEXT, `created` INTEGER, `data` BLOB, PRIMARY KEY(`hash`) )")
	db.Exec("CREATE TABLE IF NOT EXISTS `hashes` ( `hash` TEXT, `type` TEXT, PRIMARY KEY(`hash`) )")
	db.Exec("CREATE TABLE IF NOT EXISTS `targets` ( `hash` TEXT, `name` TEXT, PRIMARY KEY(`hash`) )")
	db.Exec("CREATE TABLE IF NOT EXISTS `documentTargets` ( `documentHash` TEXT, `targetHash` TEXT )")
	db.Exec("CREATE UNIQUE INDEX IF NOT EXISTS  `documentTarget` ON `documentTargets` ( `documentHash`, `targetHash` )")

	if row := db.QueryRow("SELECT version FROM version;"); row.Scan(&adapter.version) == sql.ErrNoRows {
		adapter.version = 1
		db.Exec("INSERT INTO version (version) VALUES(1)")
	}

	adapter.client = db

	return nil
}
func (adapter *SQLiteStorageAdapter) Shutdown() error {
	return nil
}
func (adapter *SQLiteStorageAdapter) GetHashType(hash string) (string, error) {
	row := adapter.client.QueryRow("SELECT type FROM hashes WHERE hash=?", hash)
	var hashType string
	if err := row.Scan(&hashType); err != nil {
		return "", err
	}
	return hashType, nil
}
func (adapter *SQLiteStorageAdapter) GetTargetName(hash string) (string, error) {
	row := adapter.client.QueryRow("SELECT name FROM targets WHERE hash=?", hash)
	var targetName string
	if err := row.Scan(&targetName); err != nil {
		return "", err
	}
	return targetName, nil
}
func (adapter *SQLiteStorageAdapter) GetAllTargetNames() (TargetList, error) {
	rows, err := adapter.client.Query("SELECT name FROM targets")
	if err != nil {
		return nil, err
	}

	var result TargetList = make(TargetList, 0)
	for rows.Next() {
		var targetName string
		if err := rows.Scan(&targetName); err != nil {
			return nil, err
		}
		result = append(result, targetName)
	}
	return result, nil
}
func (adapter *SQLiteStorageAdapter) GetAllDocuments(startTime int64, endTime int64, limit int) ([]string, error) {
	if endTime == 0 {
		endTime = math.MaxInt64
	}
	if limit == 0 {
		limit = math.MaxInt32
	}

	rows, err := adapter.client.Query("SELECT hash FROM documents d WHERE d.created>=? AND d.created<? ORDER BY d.created DESC LIMIT ?", startTime, endTime, limit)
	if err != nil {
		return nil, err
	}

	var result []string = make([]string, 0)
	for rows.Next() {
		var targetHash string
		if err := rows.Scan(&targetHash); err != nil {
			return nil, err
		}
		result = append(result, targetHash)
	}
	return result, nil
}
func (adapter *SQLiteStorageAdapter) GetDocumentsForTarget(targetHash string, startTime int64, endTime int64, limit int) ([]string, error) {
	if endTime == 0 {
		endTime = math.MaxInt64
	}
	if limit == 0 {
		limit = math.MaxInt32
	}

	var err error
	var rows *sql.Rows
	if targetHash == "" {
		rows, err = adapter.client.Query("SELECT hash FROM documents d WHERE ((SELECT 1 FROM documentTargets dt WHERE dt.documentHash=d.hash) IS NULL) AND d.created>=? AND d.created<? ORDER BY d.created DESC LIMIT ?", startTime, endTime, limit)
	} else {
		rows, err = adapter.client.Query("SELECT dt.documentHash FROM documentTargets dt JOIN documents d ON d.hash=dt.documentHash WHERE dt.targetHash=? AND d.created>=? AND d.created<? ORDER BY d.created DESC LIMIT ?", targetHash, startTime, endTime, limit)
	}
	if err != nil {
		return nil, err
	}

	var result []string = make([]string, 0)
	for rows.Next() {
		var targetHash string
		if err := rows.Scan(&targetHash); err != nil {
			return nil, err
		}
		result = append(result, targetHash)
	}
	return result, nil
}
func (adapter *SQLiteStorageAdapter) UpdateTargetHash(oldHash string, newHash string) error {
	if _, err := adapter.client.Exec("UPDATE documentTargets SET targetHash=? WHERE targetHash=?", newHash, oldHash); err != nil {
		return err
	}
	if _, err := adapter.client.Exec("UPDATE targets SET hash=? WHERE hash=?", newHash, oldHash); err != nil {
		return err
	}
	if _, err := adapter.client.Exec("UPDATE hashes SET hash=? WHERE hash=?", newHash, oldHash); err != nil {
		return err
	}
	return nil
}
func (adapter *SQLiteStorageAdapter) ReadDocument(hash string) (*Document, error) {
	row := adapter.client.QueryRow("SELECT data FROM documents WHERE hash=?", hash)

	var data []byte
	if err := row.Scan(&data); err != nil {
		if err == sql.ErrNoRows {
			return nil, ErrDocumentNotFound
		}
		return nil, err
	}

	var document *Document = &Document{}
	err := json.Unmarshal(data, document)
	if err != nil {
		return nil, err
	}

	return document, nil
}
func (adapter *SQLiteStorageAdapter) WriteDocument(hash string, document *Document) error {
	data, err := json.Marshal(document)
	if err != nil {
		return ErrMarshalDocument
	}

	if _, err := adapter.client.Exec("INSERT OR REPLACE INTO documents (hash, created, data) VALUES(?, ?, ?)", hash, document.Created.Unix(), data); err != nil {
		return err
	}
	if _, err := adapter.client.Exec("DELETE FROM documentTargets WHERE documentHash=?", hash); err != nil {
		return err
	}
	if _, err := adapter.client.Exec("INSERT OR IGNORE INTO hashes (hash, type) VALUES(?, 'document')", hash); err != nil {
		return err
	}
	for _, targetName := range document.Targets {
		targetHash := getTargetHash(targetName)
		if _, err := adapter.client.Exec("INSERT OR IGNORE INTO documentTargets (documentHash, targetHash) VALUES(?,?)", hash, targetHash); err != nil {
			return err
		}
		if _, err := adapter.client.Exec("INSERT OR REPLACE INTO targets (hash, name) VALUES(?,?)", targetHash, targetName); err != nil {
			return err
		}
		if _, err := adapter.client.Exec("INSERT OR IGNORE INTO hashes (hash, type) VALUES(?, 'target')", targetHash); err != nil {
			return err
		}
	}

	adapter.checkForAndDeleteUnusedTargets()

	return nil
}
func (adapter *SQLiteStorageAdapter) DeleteDocument(hash string) error {
	if _, err := adapter.client.Exec("DELETE FROM documents WHERE hash=?", hash); err != nil {
		return err
	}
	if _, err := adapter.client.Exec("DELETE FROM documentTargets WHERE documentHash=?", hash); err != nil {
		return err
	}

	adapter.checkForAndDeleteUnusedTargets()

	return nil
}
