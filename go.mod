module gitlab.com/uranoxyd/flustro

go 1.16

require (
	github.com/gomarkdown/markdown v0.0.0-20210514010506-3b9f47219fe7
	github.com/gorilla/mux v1.8.0
	github.com/mitchellh/mapstructure v1.4.1
	github.com/sirupsen/logrus v1.8.1
	gitlab.com/uranoxyd/mergingfs v0.1.0
	gopkg.in/yaml.v2 v2.4.0
	modernc.org/sqlite v1.11.2
)
