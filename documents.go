// Copyright (c) 2021 David Ewelt <uranoxyd@gmail.com>, released under MIT License. See LICENSE file.

package main

import (
	"fmt"
	"sort"
	"strings"
	"time"

	"github.com/gomarkdown/markdown"
	"github.com/gomarkdown/markdown/parser"
)

type TargetList []string

func (targets *TargetList) Add(target string) {
	if targets.Contains(target) {
		return
	}
	//targets = append(targets, target)
	*targets = append(*targets, target)
}
func (targets *TargetList) Remove(target string) {
	index := targets.Index(target)
	if index == -1 {
		return
	}
	*targets = append((*targets)[:index], (*targets)[index+1:]...)
}
func (targets TargetList) Join() string {
	return strings.Join(targets, ", ")
}
func (targets TargetList) Index(target string) int {
	for i, t := range targets {
		if strings.EqualFold(t, target) {
			return i
		}
	}
	return -1
}
func (targets TargetList) Contains(target string) bool {
	for _, t := range targets {
		if strings.EqualFold(t, target) {
			return true
		}
	}
	return false
}
func (targets TargetList) Sort() TargetList {
	sort.Slice(targets, func(i, j int) bool {
		return targets[i] < targets[j]
	})
	return targets
}
func (targets TargetList) Sorted() TargetList {
	result := make(TargetList, len(targets))
	copy(result, targets)
	return result.Sort()
}

type Document struct {
	Hash       string     `json:"hash"`
	Created    time.Time  `json:"created"`
	LastUpdate time.Time  `json:"updated"`
	Title      string     `json:"title"`
	Body       string     `json:"body"`
	Targets    TargetList `json:"targets"`
}

type DocumentDateGroup struct {
	Date      time.Time
	Documents []*Document
}

func (doc *Document) GetHash() string {
	if doc.Hash != "" {
		return doc.Hash
	}
	hash := getSimpleHash(fmt.Sprint(doc.Created.UnixNano()))
	doc.Hash = hash
	return hash
}

func (doc *Document) Save() error {
	return storage.SaveDocument(doc)
}
func (doc *Document) Delete() error {
	return storage.DeleteDocument(doc)
}

func (doc *Document) BodyHTML() string {
	markdownExtensions := parser.CommonExtensions | parser.HardLineBreak | parser.Autolink | parser.FencedCode
	markdownParser := parser.NewWithExtensions(markdownExtensions)
	return string(markdown.ToHTML([]byte(doc.Body), markdownParser, nil))
}
func (doc *Document) BodyHTMLNoLinks() string {
	markdownExtensions := parser.HardLineBreak | parser.FencedCode
	markdownParser := parser.NewWithExtensions(markdownExtensions)
	return string(markdown.ToHTML([]byte(doc.Body), markdownParser, nil))
}
