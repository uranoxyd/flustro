// Copyright (c) 2021 David Ewelt <uranoxyd@gmail.com>, released under MIT License. See LICENSE file.

package main

import (
	"fmt"
	"io/fs"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

type Config struct {
	LogLevel           string            `yaml:"log-level"`
	Frontend           *FrontendConfig   `yaml:"frontend"`
	Secret             string            `yaml:"secret"`
	Storage            *StorageConfig    `yaml:"storage"`
	Editors            map[string]string `yaml:"editors"`
	AllowMasterFeedFor []string          `yaml:"allow-masterfeed-for"`
	PrivacyLost        map[string]int    `yaml:"privacy-lost"`
}

type FrontendConfig struct {
	ReparseTemplates bool                `yaml:"reparse-templates"`
	ServerAddress    string              `yaml:"server-address"`
	StartBrowser     bool                `yaml:"start-browser"`
	URL              string              `yaml:"url"`
	HTML             *FrontendHTMLConfig `yaml:"html"`
}

type FrontendHTMLConfig struct {
	MaxEntriesOnOverview int `yaml:"max-entries-on-overview"`
}

type StorageConfig struct {
	MaxTargets     int         `yaml:"max-targets"`
	MaxTitleLength int         `yaml:"max-title-length"`
	MaxBodyLength  int         `yaml:"max-body-length"`
	Adapter        string      `yaml:"adapter"`
	Config         interface{} `yaml:"config"`
}

var DefaultConfig = &Config{
	LogLevel: "debug",
}

var config *Config = DefaultConfig
var noConfigWasLoaded = true

func configApplyConfD(root string) {
	if _, err := os.Stat(root); os.IsNotExist(err) {
		return
	}

	filepath.WalkDir(root, func(path string, d fs.DirEntry, err error) error {
		if d.IsDir() {
			return nil
		}

		if !strings.HasSuffix(path, ".yaml") {
			return nil
		}

		log.Debug("trying to merge config: ", path)
		if err := configMergeConfig(path); err != nil {
			log.Errorf("error loading config '%s': %s", path, err)
			os.Exit(1)
		}

		return nil
	})
}

func configTryMergeConfig(path string) {
	file := filepath.Join(path, "config.yaml")

	if _, err := os.Stat(file); os.IsNotExist(err) {
		return
	}

	makeAbsPath(&file)

	log.Debug("trying to merge config: ", file)
	if err := configMergeConfig(file); err != nil {
		log.Errorf("error loading config '%s': %s", file, err)
		os.Exit(1)
	}

	configApplyConfD(filepath.Join(path, "conf.d"))
}

func configMergeConfig(path string) error {
	bytes, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}

	if err := yaml.Unmarshal(bytes, config); err != nil {
		return err
	}

	noConfigWasLoaded = false

	return nil
}

func configSanityCheck() bool {
	if config.Secret == "" {
		secret := createSecret()
		log.Warn("!WARNING! No secret is set in the configuration, this is a significant security risk for the privacy of your flustris.")
		log.Warn("Please set the configuration value to some long random text")
		log.Warn("Here is one randomly created you can use: " + secret)
	}
	if config.Editors == nil || len(config.Editors) == 0 {
		log.Warnf("you did not configured a least one editor user")
	}
	if config.Frontend == nil {
		log.Fatal("config is missing frontend config")
		return false
	}
	if config.Frontend.HTML == nil {
		log.Fatal("config is missing frontend.html config")
		return false
	}
	if config.Frontend.ServerAddress == "" {
		log.Fatal("config value for frontend.server-address is not set")
		return false
	}
	if config.Frontend.URL == "" {
		log.Warnf("config value for frontend.url is not set, i use http://%s instead", config.Frontend.ServerAddress)
		config.Frontend.URL = fmt.Sprintf("http://%s", config.Frontend.ServerAddress)
	}
	if config.Storage == nil {
		log.Fatal("config value for frontend.stoarage is not set")
		return false
	}
	if config.PrivacyLost == nil {
		config.PrivacyLost = make(map[string]int)
	} else {
		var privacyLostMap map[string]int = make(map[string]int)
		for k, v := range config.PrivacyLost {
			privacyLostMap[strings.ToLower(k)] = v
		}
		config.PrivacyLost = privacyLostMap
	}
	return true
}
