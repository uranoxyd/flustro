// Copyright (c) 2021 David Ewelt <uranoxyd@gmail.com>, released under MIT License. See LICENSE file.

package main

import (
	"crypto/md5"
	"crypto/rand"
	"crypto/sha1"
	"crypto/sha256"
	"encoding/base32"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"io"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"
)

func makeAbsPath(path *string) {
	new, err := filepath.Abs(*path)
	if err != nil {
		return
	}
	*path = new
}

func openBrowser(url string) error {
	var err error

	switch runtime.GOOS {
	case "linux":
		err = exec.Command("xdg-open", url).Start()
	case "windows":
		err = exec.Command("rundll32", "url.dll,FileProtocolHandler", url).Start()
	case "darwin":
		err = exec.Command("open", url).Start()
	default:
		err = fmt.Errorf("unsupported platform")
	}

	return err
}

func createSecret() string {
	var b []byte = make([]byte, 256)
	rand.Read(b)
	h := sha256.New()
	h.Write(b)
	return strings.TrimRight(base64.StdEncoding.EncodeToString(h.Sum(nil)), "=")
}
func getHexSHA1(input string) string {
	h := sha1.New()
	io.WriteString(h, input)
	return hex.EncodeToString(h.Sum(nil))
}
func getTargetHashWithPrivacyLostIteration(targetName string, privacyLostIteration int) string {
	return getSimpleHash(fmt.Sprint(privacyLostIteration) + strings.ToLower(targetName))
}
func getTargetHash(targetName string) string {
	targetName = strings.ToLower(targetName)
	if pli, found := config.PrivacyLost[targetName]; found {
		return getTargetHashWithPrivacyLostIteration(targetName, pli)
	}
	return getTargetHashWithPrivacyLostIteration(targetName, 0)
}
func getSimpleHash(input string) string {
	h := md5.New()
	io.WriteString(h, config.Secret+input)
	return strings.ToLower(strings.TrimRight(base32.StdEncoding.EncodeToString(h.Sum(nil)), "="))
}
