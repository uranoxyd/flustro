# Flustro

Flustro could be described as a micro-blogging software.

[Here you can try a demo](https://demo.flustro.ewelt.net) (username: demo, password: demo)

# Idea

Flustro was born out of the need to share links or thoughts with friends and people I know on a private level, without having to write to them directly. Instead of bombarding each person at the next time i meet them, with content that I may have forgotten, it made more sense to let people decide for themselves when they wanted to receive and view that content. Also, the goal was to have sovereignty over one's own data, i.e. be self-hosted. Furthermore, it shouldn't be necessary for recipients to log in somewhere to receive the content and it should be available via RSS reader.


# Features

* Easy to install
* Minimalistic concept and user interface
* Content can be assigned to one or more destinations
* Recipients do not need to be logged in to retrieve content
* RSS/Atom feeds of content per recipient
* Individual posts can be forwarded without the respective feed being visible
* Platform independent through web UI
* Text can be formatted with Markdown Syntax


# Installation

Just download the executable for your operating system and run it, that's it.

Later you may want to configure something, for that you can use the ``config.example.yaml`` as a reference. It is sufficient to take single values from the example configuration, you do not have to take the whole configuration.

For example, if you want your browser to open automatically when you start Flustro, the configuration could look like this:

```yaml
frontend:
    start-browser: yes
```

# Usage

Create a user for yourself in the configuration:

```yaml
editors:
    Your-username: Your-password
```

Start Flustro, navigate to ``http://127.0.0.1:8484`` and log in with your credentials.

Now try to create an entry, give it a title and a body and assign it to a target,
Targets are the people you want to give access to this post. You don't need to register them, just enter a name in the input field and submit the post.

On the main page you will now see the target(s) you entered, and clicking on the name will take you to the feed for that person. The URL that is now in your address bar is the URL you can give to the person this feed is for.

You can append ``.rss`` or ``.atom`` to this URL to get an RSS or Atom feed.

If you click on the ``[L]`` next to an entry in the feed, you will be redirected to a special page showing only that entry. The address in the address bar is also a perma-link to that entry, the people who have that entry associated with them can safely share that link with other people without giving them access to their private feed.


# State of the project

Flustro is feature complete. I will still work on the user interface to make it a bit nicer and especially mobile friendly and fix bugs. Nevertheless I am happy about feature requests, bugreports and pull-requests.


# License
MIT
