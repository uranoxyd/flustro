// Copyright (c) 2021 David Ewelt <uranoxyd@gmail.com>, released under MIT License. See LICENSE file.

package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"

	log "github.com/sirupsen/logrus"
)

var exitSignalChannel chan os.Signal

func main() {
	loggingInit()

	config = DefaultConfig
	configTryMergeConfig("/etc/edx")
	configTryMergeConfig("./")
	if path, err := os.UserConfigDir(); err == nil {
		configTryMergeConfig(filepath.Join(path, "edx"))
	}
	if path, err := os.UserHomeDir(); err == nil {
		configTryMergeConfig(filepath.Join(path, ".edx"))
	}

	if noConfigWasLoaded {
		config.Secret = createSecret()
		ioutil.WriteFile("config.yaml", []byte(fmt.Sprintf("secret: %s\n", config.Secret)), os.ModePerm)
	}

	loggingMain()
	configSanityCheck()

	storageMain()
	storage.Startup()

	for targetName, pli := range config.PrivacyLost {
		for i := 1; i <= pli; i++ {
			oldHash := getTargetHashWithPrivacyLostIteration(targetName, i-1)
			newHash := getTargetHashWithPrivacyLostIteration(targetName, i)
			storage.UpdateTargetHash(oldHash, newHash)
		}
	}

	frontendMain()

	//-- wait for SIGINT
	log.Info("press CTRL-C to exit")
	exitSignalChannel = make(chan os.Signal, 1)
	signal.Notify(exitSignalChannel, syscall.SIGTERM, syscall.SIGINT)
	signal := <-exitSignalChannel

	if signal == syscall.SIGTERM {
		log.Info("received SIGTERM signal")
	} else if signal == syscall.SIGINT {
		log.Info("received SIGINT signal")
	}

	storage.Shutdown()
}
