// Copyright (c) 2021 David Ewelt <uranoxyd@gmail.com>, released under MIT License. See LICENSE file.

package main

import (
	"embed"
	"fmt"
	"html"
	"io/fs"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/uranoxyd/flustro/frontend"
	"gitlab.com/uranoxyd/mergingfs"
)

//go:embed static
var frontendStaticEmbedFS embed.FS

//go:embed templates
var frontendTemplatesEmbedFS embed.FS

var frontendStaticFS *mergingfs.MergingFS
var frontendTemplatesFS *mergingfs.MergingFS

var frontendServer *frontend.FrontendServer

func init() {
	DefaultConfig.Frontend = &FrontendConfig{
		ServerAddress:    "127.0.0.1:8484",
		URL:              "http://127.0.0.1:8484",
		ReparseTemplates: true,
		HTML: &FrontendHTMLConfig{
			MaxEntriesOnOverview: 25,
		},
	}
}

func checkCredentials(username string, password string) bool {
	configPassword, userFound := config.Editors[username]
	if !userFound {
		return false
	}

	if !strings.HasPrefix(configPassword, "sha1:") {
		configPassword = getHexSHA1(configPassword)
	} else {
		configPassword = configPassword[5:]
	}

	return configPassword == password
}

func frontendCheckAuthorization(r *http.Request) string {
	cookie, err := r.Cookie("flustro-login")
	if err != nil {
		return ""
	}

	login := cookie.Value
	if login == "" {
		return ""
	}

	colon := strings.Index(login, ":")
	username := login[:colon]
	passwordHash := login[colon+1:]

	if checkCredentials(username, passwordHash) {
		return username
	}
	return ""
}

func frontendRequireAuthorization(w http.ResponseWriter, r *http.Request, path string) string {
	if username := frontendCheckAuthorization(r); username != "" {
		return username
	}

	username, password, hasBasicAuth := r.BasicAuth()
	if hasBasicAuth && checkCredentials(username, getHexSHA1(password)) {
		cookie := &http.Cookie{Name: "flustro-login", Value: fmt.Sprintf("%s:%s", username, getHexSHA1(password)), Expires: time.Now().Add(356 * 24 * time.Hour), Secure: true}
		http.SetCookie(w, cookie)

		if path != "" {
			w.Header().Add("Location", path)
			w.WriteHeader(http.StatusSeeOther)
		}
		return username
	} else if hasBasicAuth {
		colon := strings.LastIndex(r.RemoteAddr, ":")
		ip := r.RemoteAddr[:colon]
		port, _ := strconv.Atoi(r.RemoteAddr[colon+1:])
		log.Warnf("failed login attempt from %s on port %d", ip, port)
	}

	username = frontendCheckAuthorization(r)

	if username == "" {
		w.Header().Set("WWW-Authenticate", "Basic realm=\"Authorization required\"")
		w.WriteHeader(http.StatusUnauthorized)
		return ""
	}

	if path != "" {
		w.Header().Add("Location", path)
		w.WriteHeader(http.StatusSeeOther)
	}
	return username
}

func frontendRunServer() error {
	router := mux.NewRouter()

	router.HandleFunc("/", frontendIndex)
	router.HandleFunc("/login", frontendLogin)
	router.HandleFunc("/logout", frontendLogout)
	router.HandleFunc("/master-feed.{format}", frontendMasterFeed)
	router.HandleFunc("/write", frontendEdit)
	router.HandleFunc("/{hash}/edit", frontendEdit)
	router.HandleFunc("/{hash}/delete", frontendDelete)
	router.HandleFunc("/{query}", frontendQuery)
	router.HandleFunc("/{query}/{year:[0-9]+}/{month:[0-9]+}", frontendQuery)
	router.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.FS(frontendStaticFS))))

	frontendServer = frontend.NewFrontendServer(config.Frontend.ServerAddress, frontendTemplatesFS, router)
	frontendServer.AddTemplateFunction("join", strings.Join)
	frontendServer.AddTemplateFunction("targetHash", getTargetHash)
	frontendServer.AddTemplateFunction("escape", html.EscapeString)

	var err error
	go func() {
		err = frontendServer.Start()
	}()
	if err != nil {
		log.Errorf("frontend server start error: %s", err)
	} else {
		log.Infof("frontend server started under http://%s", config.Frontend.ServerAddress)
		if config.Frontend.StartBrowser {
			if err := openBrowser("http://" + config.Frontend.ServerAddress); err != nil {
				log.Error("could not start browser: %v", err)
			}
		}
	}
	return err
}

func frontendMain() {
	sefs, err := fs.Sub(frontendStaticEmbedFS, "static")
	if err != nil {
		log.Error("error with static embed fs:", err)
		os.Exit(1)
	}
	frontendStaticFS = mergingfs.NewMergingFS(os.DirFS("static"), sefs)

	tefs, err := fs.Sub(frontendTemplatesEmbedFS, "templates")
	if err != nil {
		log.Error("error with templates embed fs:", err)
		os.Exit(1)
	}
	frontendTemplatesFS = mergingfs.NewMergingFS(os.DirFS("templates"), tefs)

	frontendRunServer()
}
